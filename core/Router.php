<?php

set_error_handler('exceptions_error_handler');

function exceptions_error_handler($severity, $message, $filename, $lineno) {
		if (error_reporting() == 0) {
			return;
		}
		if (error_reporting() & $severity) {
			throw new ErrorException($message, 0, $severity, $filename, $lineno);
		}
  }

class Router{

	private $request;
	private $servicename;

	public function __construct($request){
		$this->request = $request;
	}

	public function map($route, $file){

		$uri = trim( $this->request, "/" );
		$uri = explode("/", $uri);
		$this->servicename = SERVICENAME;
	
		
		if (in_array ( $this->servicename, $uri)){
			$index = array_search($this->servicename,$uri) +1;
			
			if($uri[$index] == trim($route, "/")){

				$values = $this->getDataFromUrl ($uri, $index);
				if ($values[1]){
					$args= $values [0];
					require $file . '.php';
				}
				return true;
			}
		}
		return false;
	}
	public function getDataFromUrl ($uri, $startindex){
		$data = Array();
		$validation = false;
		$startindex++;
		try {
			for ($i = $startindex; $i < count ($uri); $i=$i+2){
				$data [$uri[$i]] = $uri[$i+1];
			}
			$validation = true;
		}catch (Exception $e) {
			die(Message::showMessage(ERROR,"Error in defining the parameters. Example '... / id / 56784 / ...' to each parameter must match a value."));
	  	}
		return array($data,$validation);
	}

	public static function getParamsForService($HTTPPARAMS = ""){

		$parameters = Array();
		$parameters ["CALL"] = $_SERVER['REQUEST_METHOD'];
		$parameters ["HTTPPARAMS"] = $HTTPPARAMS;
		$parameters ["POSTPARAMS"] = $_POST;

		return $parameters;

	}

}