<?php


class Authorization 
{

    private $token;


    private function getAuthorizationHeader(){
        $headers = null;
        if (isset($_SERVER['Authorization'])) {
            $this->headers = trim($_SERVER["Authorization"]);
        }
        else if (isset($_SERVER['HTTP_AUTHORIZATION'])) { 
            $headers = trim($_SERVER["HTTP_AUTHORIZATION"]);
        } elseif (function_exists('apache_request_headers')) {
            $requestHeaders = apache_request_headers();
            $requestHeaders = array_combine(array_map('ucwords', array_keys($requestHeaders)), array_values($requestHeaders));
            if (isset($requestHeaders['Authorization'])) {
                $headers = trim($requestHeaders['Authorization']);
            }
        }
        return $headers;
    }

    private function getBearerToken() {
        $headers = $this->getAuthorizationHeader();
        if (!empty($headers)) {
            if (preg_match('/Bearer\s(\S+)/', $headers, $matches)) {
                return $matches[1];
                
            }
        }
        return null;
    }

    private function validToken($token){
        if ($token == AUTHTOKEN){
            return true;
        }
        return false;
    }

    public function getAuthorization($type){
        
        switch ($type) {
            case AUTHTYPE:
                return $this->validToken($this->getBearerToken()) ? false : true;
            default:
                die(Message::showMessage(ERROR,"No Authorization parameter passed"));
        }
    }


}