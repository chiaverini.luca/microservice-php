<?php


class Message{

    public static function  showMessage($type, $message = 'No message to display.'){

        $messageArray = Array();

        switch ($type) {
            case ERROR:
                $messageArray ['status'] = ERROR;
                break;
            case SUCCESS:
                $messageArray ['status'] = SUCCESS;
                break;
            default:
                $messageArray ['status'] = ERROR;
        }

        $messageArray ['messagge'] = $message;
        echo json_encode ($messageArray);

    }

}

?>