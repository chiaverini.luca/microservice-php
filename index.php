<?php
require_once ('core/Router.php');
require_once ('core/Config.php');
require_once ('core/Message.php');
require_once ('core/Authorization.php');

if((new Authorization)->getAuthorization(AUTHTYPE) && AUTHSTATUS){
    header('WWW-Authenticate: Basic realm="Restricted area"');
    header('HTTP/1.0 401 Unauthorized');
    die(Message::showMessage(ERROR,"Unauthorized action"));
}



$router = new Router($_SERVER['REQUEST_URI']);

//Mapping URL

$callMethodResult = $router->map('CustomService', 'app/CustomService', $_POST);


//END Mapping URL

if (!$callMethodResult){
    die(Message::showMessage(ERROR,"The Method called is wrong or not exist OR Define at least one method to call"));
}



